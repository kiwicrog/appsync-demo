// Run `sls deploy` before running tests to generate this JSON file with AWS configuration
import awsStack from '../../../aws-stack.json';

const url = Cypress.env('localGraphQlApiUrl') || awsStack.GraphQlApiUrl;
const profile = awsStack.Profile;
const region = awsStack.Region;
const userPoolId = awsStack.UserPoolId;
const clientId = awsStack.UserPoolClientId;
const username = 'admin@example.com';
const password = 'password123';


const createGraphQlRequest = (query, token) => ({
  url,
  method: "POST",
  body: JSON.stringify(query),
  headers: {
    Authorization: token,
    "content-type": "application/json"
  }
});

context('Tests', () => {
  let accessToken;

  before(() => {
    // Check that test user exists
    cy.exec(`AWS_PROFILE=${profile} aws cognito-idp admin-get-user --region ${region} --user-pool-id ${userPoolId} --username ${username}`, { failOnNonZeroExit: false })
      .then((out) => {
        if (out.stderr || JSON.parse(out.stdout).UserStatus !== 'CONFIRMED') {
          // Create test user if it doesn't already exist
          cy.exec(`AWS_PROFILE=${profile} aws cognito-idp sign-up --region ${region} --client-id ${clientId} --username ${username} --password ${password}`);

          // Confirm test user so that it can authenticate
          cy.exec(`AWS_PROFILE=${profile} aws cognito-idp admin-confirm-sign-up --region ${region} --user-pool-id ${userPoolId} --username ${username}`);
        }
      });

    // Authenticate as test user
    cy.exec(`AWS_PROFILE=${profile} aws cognito-idp admin-initiate-auth --region ${region} --user-pool-id ${userPoolId} --auth-flow ADMIN_USER_PASSWORD_AUTH --auth-parameters USERNAME=${username},PASSWORD=${password} --client-id ${clientId}`)
      .then((out) => {
        const resp = JSON.parse(out.stdout);
        console.log(resp);
        accessToken = JSON.parse(out.stdout).AuthenticationResult.AccessToken;
      });
  });

  it('Creates a message', () => {
    const request = createGraphQlRequest({
      operationName: 'createMessage',
      variables: { body: 'Hello to the World!' },
      query: `
        mutation createMessage($body: String!) {
          createMessage(body: $body) {
            body
          }
        }`
    }, accessToken);

    cy.request(request).as("response");
    cy.get("@response").its('status').should("eq", 200);
    cy.get("@response").its('body.data.createMessage.body').should("eq", "Hello to the World!");
  });


  it('Gets messages', () => {
    const request = createGraphQlRequest({
      operationName: 'getMessages',
      variables: {},
      query: `
        query getMessages {
          getMessages {
            body
          }
        }`
    }, accessToken);

    cy.request(request).as("response");
    cy.get("@response").its('status').should("eq", 200);
    cy.get("@response").its('body.data.getMessages.length').should("gt", 0);
    cy.get("@response").its('body.data.getMessages').its(0).should("deep.equal", { body: "Hello to the World!" });
  });

  it('Creates a contact', () => {
    const request = createGraphQlRequest({
      operationName: 'createContact',
      variables: { name: 'Bob' },
      query: `
        mutation createContact($name: String!) {
          createContact(name: $name) {
            name
          }
        }`
    }, accessToken);

    cy.request(request).as("response");
    cy.get("@response").its('status').should("eq", 200);
    cy.get("@response").its('body.data.createContact.name').should("eq", "Bob");
  });

  it('Gets contacts', () => {
    const request = createGraphQlRequest({
      operationName: 'getContacts',
      variables: {},
      query: `
        query getContacts {
          getContacts {
            name
          }
        }`
    }, accessToken);

    cy.request(request).as("response");
    cy.get("@response").its('status').should("eq", 200);
    cy.get("@response").its('body.data.getContacts.length').should("gt", 0);
  });
});