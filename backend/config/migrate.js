// This file is generated post Serverless deployment
const awsStack = require('../../aws-stack.json');

const profile = awsStack.Profile;
const region = awsStack.Region;
const resourceArn = awsStack.AuroraClusterArn;
const secretArn = awsStack.AuroraSecretArn;
const databaseName = awsStack.AuroraDatabaseName;

const { exec } = require("child_process");
const util = require('util');

const commands = require('../src/migrations');

const generateExecuteStatementCommand = ({ sql }) => `AWS_PROFILE=${profile} aws rds-data execute-statement \
  --region ${region}\
  --resource-arn ${resourceArn}\
  --secret-arn ${secretArn}\
  --database ${databaseName}\
  --schema "mysql"\
  --continue-after-timeout\
  --sql "${sql}"`;

const executeCommands = async () => {
  const execPromise = util.promisify(exec);

  for (sql of commands) {
    const script = generateExecuteStatementCommand({ sql });

    console.log("\n" + `Running command "${sql}"`);
    await execPromise(script)
      .then(console.log)
      .catch((err) => { throw new Error(err) });
  }
};

executeCommands();