###`schema.graphql`  
The GraphQL schema for the AWS AppSync hosted API


###`/dynamodb-tables`  
Definitions of DynamoDB tables for your backend.

Any new files in this directory that describe additional DynamoDB tables must be
referenced in `serverless.yml` at the project root (you can follow the example of
MessagesTable.yml near the bottom of the file)


###`./mapping-templates`  
The mapping templates that AWS AppSync will use to resolve your GraphQL operations.

These are written in Apache Velocity Template Language (VTL).
https://docs.aws.amazon.com/appsync/latest/devguide/resolver-mapping-template-reference-programming-guide.html

The config for mapping templates is in `./mapping-templates/config.yml`


### `migrations.json`  
SQL statements that will be applied to the RDS Aurora Serverless database upon deployment.

These **must be idempotent**, as they will be run on every deployment.