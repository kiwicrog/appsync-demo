## AWS AppSync demo
Thanks to https://www.serverless.com/blog/building-chat-appliation-aws-appsync-serverless/

### What's this?
With one command, deploy a serverless GraphQL API (with AWS AppSync)
backed by DynamoDB and Aurora Serverless, with user authentication via AWS Cognito.

Automated integration tests run against the real API and databases with a real test user.

100% infrastructure-as-code, no manual config (apart from an IAM user for deployment).


### Getting Started
1. Install Node.js v8 or higher
1. Install Serverless Framework with `npm install -g serverless`
1. Create a new AWS IAM User under the profile `personal` and add the
   API key and Secret to Serverless via `sls config credentials --provider aws --profile personal`
1. Clone this repo to a local directory
1. Run `npm install`
1. Run `sls deploy`.  
   The service will be deployed to AWS.  
   Configuration will be saved to aws-stack.json.  
   A script will run database migrations (this script can fail, and the service deployment will still go ahead.
   If this occurs, run `npm run rds-aurora:migrate` manually)
1. Run `npm run cypress:open:aws`.
   This will run integration tests against your AWS deployed services.
   To test against local services instead, read the Offline Development section.

### Building your serverless backend
_Most_ of the time, you'll only need to edit the files in the `/src` directory
and write integration tests in the `/cypress` directory to build your serverless backend.

### Offline Development
Running emulated AWS services allows development against a local database.
At present this is AppSync and DynamoDB only (no RDS Aurora Serverless)
Also, authentication is still via AWS Cognito, requiring an internet connection.

1. Install a Java SDK
1. `sls dynamodb install`
1. `sls offline start --stage local`
1. `npx cypress open`