### Getting Started
1. Follow the instructions in `./backend/README.md` to deploy the backend
   and run the integration tests for the first time
  
1. Then, in the `/web` directory, run `npm install` followed by `npm run develop`
   to start a demo Gatsby webapp at `http://localhost:8000`
