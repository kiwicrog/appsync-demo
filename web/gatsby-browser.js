// aws-stack.json is created after first deploying the backend (with `sls deploy` command in the `/backend` directory)
import awsConfig from '../aws-stack.json';
import Amplify from "@aws-amplify/core";


Amplify.configure({
  "aws_project_region": awsConfig.Region,
  "aws_appsync_graphqlEndpoint": awsConfig.GraphQlApiUrl,
  "aws_appsync_region": awsConfig.Region,
  "aws_appsync_authenticationType": "AMAZON_COGNITO_USER_POOLS",
  Auth: {
    identityPoolId: awsConfig.IdentityPoolId,
    region: awsConfig.Region,
    userPoolId: awsConfig.UserPoolId,
    userPoolWebClientId: awsConfig.UserPoolClientId,
  }
});
