import React from 'react'

import Auth from '@aws-amplify/auth';
import Image from "../components/image"
import SEO from "../components/seo"

import { useQuery } from '@apollo/react-hooks';
import gql from 'graphql-tag';

const GET_MESSAGES = gql`
    query getMessages {
        getMessages {
            messageId
            body
        }
    }
`;

const IndexPage = () => {
  const { data } = useQuery(GET_MESSAGES, {
    context: {
      headers: {
        Authorization: Auth.user.signInUserSession.accessToken.jwtToken
      }
    }
  });

  return (
    <>
        <SEO title="Home" />
        <h1>Messages</h1>
      {data ? data.getMessages.map((d) => (<p key={d.messageId}>{d.body}</p>)) : null}
        <div style={{ maxWidth: `300px`, marginBottom: `1.45rem` }}>
          <Image />
        </div>
    </>
  );
};

export default IndexPage;
