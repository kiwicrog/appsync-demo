import React from 'react'
import { withAuthenticator } from '@aws-amplify/ui-react';

// aws-stack.json is created after first deploying the backend (with `sls deploy` command in the `/backend` directory)
import awsConfig from '../../../aws-stack.json';

import Layout from "../components/layout"
import Landing from "./landing";

import ApolloClient from 'apollo-boost';
import { ApolloProvider } from '@apollo/react-hooks';

const client = new ApolloClient({ uri: awsConfig.GraphQlApiUrl });

const App = () => {
  return (
    <ApolloProvider client={client}>
      <Layout>
        <Landing/>
      </Layout>
    </ApolloProvider>
  );
};

export default withAuthenticator(App, true);
